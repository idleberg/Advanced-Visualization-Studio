/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

enum {
    HORIZONTAL1 = 1,
    HORIZONTAL2 = 2,
    VERTICAL1 = 4,
    VERTICAL2 = 8,

    D_HORIZONTAL1 = 0,
    D_HORIZONTAL2 = 8,
    D_VERTICAL1 = 16,
    D_VERTICAL2 = 24,

    M_HORIZONTAL1 = 0xff,
    M_HORIZONTAL2 = 0xff00,
    M_VERTICAL1 = 0xff0000,
    M_VERTICAL2 = 0xff000000,
};

typedef struct {
    int frame_count;
    int enabled;
    int mode;
    int on_beat;
    int rbeat;
    int smooth;
    int slower;
    int last_mode;
    int divisors;
    int inc;
} MirrorContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    MirrorContext *mirror = ctx->priv;
    int pos = 0;
    R32(mirror->enabled);
    R32(mirror->mode);
    R32(mirror->on_beat);
    R32(mirror->smooth);
    R32(mirror->slower);
    mirror->rbeat = HORIZONTAL2;
    return 0;
}

static inline int blend_adapt(int a, int b, int divisor)
{
    return (((a >> 4) & 0x0f0f0f) * (16 - divisor)) + (((b >> 4) & 0x0f0f0f) * divisor);
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    MirrorContext *mirror = ctx->priv;
    int this_mode = mirror->mode;

    if (!mirror->enabled)
        return 0;
    if (mirror->on_beat) {
        if (is_beat)
            mirror->rbeat = (rand() % 16) & mirror->mode;
        this_mode = mirror->rbeat;
    }

    if (this_mode != mirror->last_mode) {
        int diff = this_mode ^ mirror->last_mode;
        for (int i = 1, m = 0xff, d = 0; i < 16; i <<= 1, m <<= 8, d += 8) {
            if (diff & i) {
                mirror->inc = mirror->inc & ~m;
                mirror->inc |= (mirror->last_mode & i ? 0xff : 1) << d;
                if (!(mirror->divisors & m)) {
                    mirror->divisors = mirror->divisors & ~m;
                    mirror->divisors |= (mirror->last_mode & i ? 16 : 1) << d;
                }
            }
        }
        mirror->last_mode = this_mode;
    }

    int *fbp = frame;
    if (this_mode & VERTICAL1 || (mirror->smooth && (mirror->divisors & M_VERTICAL1))) {
        int divis = (mirror->divisors & M_VERTICAL1) >> D_VERTICAL1;
        for (int hi = 0; hi < h; hi++) {
            if (mirror->smooth && divis) {
                int *tmp = fbp + w-1;
                for (int n = w/2; n; n--, tmp--, fbp++)
                    *tmp = blend_adapt(*tmp, *fbp, divis);
            } else {
                int *tmp = fbp + w-1;
                for (int n = w/2; n; n--)
                    *tmp-- = *fbp++;
            }
            fbp += w/2;
        }
    }

    fbp = frame;
    if (this_mode & VERTICAL2 || (mirror->smooth && (mirror->divisors & M_VERTICAL2))) {
        int divis = (mirror->divisors & M_VERTICAL2) >> D_VERTICAL2;
        for (int hi = 0; hi < h; hi++) {
            if (mirror->smooth && divis) {
                int *tmp = fbp + w-1;
                for (int n = w/2; n; n--, fbp++, tmp--)
                    *fbp = blend_adapt(*fbp, *tmp, divis);
            } else {
                int *tmp = fbp + w-1;
                for (int n = w/2; n; n--)
                    *fbp++ = *tmp--;
            }
            fbp += w/2;
        }
    }

    fbp = frame;
    int j = w * h - w;
    if (this_mode & HORIZONTAL1 || (mirror->smooth && (mirror->divisors & M_HORIZONTAL1))) {
        int divis = (mirror->divisors & M_HORIZONTAL1) >> D_HORIZONTAL1;
        for (int hi = 0; hi < h/2; hi++) {
            if (mirror->smooth && divis) {
                for (int n = w; n; n--, fbp++)
                    fbp[j] = blend_adapt(fbp[j], *fbp, divis);
            } else {
                memcpy(fbp + j, fbp, w * sizeof(int));
                fbp += w;
            }
            j -= w*2;
        }
    }

    fbp = frame;
    j = w * h - w;
    if (this_mode & HORIZONTAL2 || (mirror->smooth && (mirror->divisors & M_HORIZONTAL2))) {
        int divis = (mirror->divisors & M_HORIZONTAL2) >> D_HORIZONTAL2;
        for (int hi = 0; hi < h/2; hi++) {
            if (mirror->smooth && divis) {
                for (int n = w; n; n--, fbp++)
                    *fbp = blend_adapt(*fbp, fbp[j], divis);
            } else {
                memcpy(fbp, fbp + j, w * sizeof(int));
                fbp += w;
            }
            j -= w*2;
        }
    }

    if (mirror->smooth && !(++mirror->frame_count % mirror->slower))
        for (int i = 0, m = 0xff, d = 0; i < 16; i <<= 1, m <<= 8, d += 8)
            if (mirror->divisors & m) {
                mirror->divisors = mirror->divisors & ~m;
                mirror->divisors |= (((mirror->divisors & m) >> d) +
                        ((mirror->inc & m) >> d) % 16) << d;
            }

    return 0;
}

Component t_mirror = {
    .name = "Trans / Mirror",
    .code = 26,
    .priv_size = sizeof(MirrorContext),
    .load_config = load_config,
    .render = render,
};
