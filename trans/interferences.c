/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "block.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int enabled;
    int n_points;
    int distance;
    int alpha;
    int rotation;
    int rotation_inc;
    int distance2;
    int alpha2;
    int rotation_inc2;
    int rgb;
    int blend, blend_avg;
    float speed;
    int onbeat;
    //float a;
    //int _distance;
    //int _alpha;
    //int _rotation_inc;
    //int _rgb;
    float status;
} InterferencesContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    InterferencesContext *ictx = ctx->priv;
    int pos = 0;

    R32(ictx->enabled);
    R32(ictx->n_points);
    R32(ictx->rotation);
    R32(ictx->distance);
    R32(ictx->alpha);
    R32(ictx->rotation_inc);
    R32(ictx->blend);
    R32(ictx->blend_avg);
    R32(ictx->distance2);
    R32(ictx->alpha2);
    R32(ictx->rotation_inc2);
    R32(ictx->rgb);
    R32(ictx->onbeat);
    R32F(ictx->speed);

    //ictx->a = ictx->rotation / 255.0 * M_PI * 2;
    ictx->status = M_PI;

    return 0;
}

#define MAX_POINTS 8

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    InterferencesContext *ictx = ctx->priv;

    if (!ictx->enabled)
        return 0;

    if (!ictx->n_points)
        return 0;

    if (is_beat && ictx->onbeat && ictx->status >= M_PI)
        ictx->status = 0;

    float a = ictx->rotation / 255.0 * M_PI * 2;
    int x_points[MAX_POINTS], y_points[MAX_POINTS];
    int x_min = 0, x_max = 0;
    int y_min = 0, y_max = 0;

    float s = sinf(ictx->status);
    int distance = ictx->distance + ((ictx->distance2 - ictx->distance) * s);

    for (int i = 0; i < ictx->n_points; i++) {
        int x = cosf(a) * distance;
        int y = sinf(a) * distance;

        if (y > y_min)
            y_min = y;
        if (-y > y_max)
            y_max = -y;

        if (x > x_min)
            x_min = x;
        if (-x > x_max)
            x_max = x;

        x_points[i] = x;
        y_points[i] = y;

        a += 2 * M_PI / ictx->n_points;
    }

    uint8_t *bt = blend_table[(int)(ictx->alpha + ((ictx->alpha2 - ictx->alpha) * s))];

    for (int y = 0; y < h; y++) {
        int y_offs[MAX_POINTS];

        for (int i = 0; i < ictx->n_points; i++) {
            if (y >= y_points[i] && y_points[i] < h)
                y_offs[i] = (y - y_points[i]) * w;
            else
                y_offs[i] = -1;
        }

        if (ictx->rgb && ictx->n_points == 3) {
            for (int x = 0; x < w; x++) {
                int r = 0, g = 0, b = 0;
                int xp;

                xp = x - x_points[0];
                if (xp >= 0 && xp < w && y_offs[0] != -1)
                    r = bt[CR_TO_I(frame[xp + y_offs[0]])];
                xp = x - x_points[1];
                if (xp >= 0 && xp < w && y_offs[1] != -1)
                    g = bt[CG_TO_I(frame[xp + y_offs[1]])];
                xp = x - x_points[2];
                if (xp >= 0 && xp < w && y_offs[2] != -1)
                    b = bt[CB_TO_I(frame[xp + y_offs[2]])];

                frame_out[y*w+x] = I3_TO_I(r,g,b);
            }
        } else if (ictx->rgb && ictx->n_points == 6) {
            for (int x = 0; x < w; x++) {
                int r = 0, g = 0, b = 0;
                int xp;

                xp = x - x_points[0];
                if (xp >= 0 && xp < w && y_offs[0] != -1)
                    r = bt[CR_TO_I(frame[xp + y_offs[0]])];
                xp = x - x_points[1];
                if (xp >= 0 && xp < w && y_offs[1] != -1)
                    g = bt[CG_TO_I(frame[xp + y_offs[1]])];
                xp = x - x_points[2];
                if (xp >= 0 && xp < w && y_offs[2] != -1)
                    b = bt[CB_TO_I(frame[xp + y_offs[2]])];

                xp = x - x_points[3];
                if (xp >= 0 && xp < w && y_offs[3] != -1)
                    r += bt[CR_TO_I(frame[xp + y_offs[3]])];
                xp = x - x_points[4];
                if (xp >= 0 && xp < w && y_offs[4] != -1)
                    g += bt[CG_TO_I(frame[xp + y_offs[4]])];
                xp = x - x_points[5];
                if (xp >= 0 && xp < w && y_offs[5] != -1)
                    b += bt[CB_TO_I(frame[xp + y_offs[5]])];

                r = MIN(r, 255);
                g = MIN(g, 255);
                b = MIN(b, 255);
                frame_out[y*w+x] = I3_TO_I(r,g,b);
            }
        } else if (y > y_min && y < h - y_max && x_min + x_max < w) {
            int x;

            for (x = 0; x < x_min; x++) {
                int r = 0, g = 0, b = 0;
                for (int i = 0; i < ictx->n_points; i++) {
                    int xp = x - x_points[i];
                    if (xp >= 0 && xp < w) {
                        r += bt[CR_TO_I(frame[xp + y_offs[i]])];
                        g += bt[CG_TO_I(frame[xp + y_offs[i]])];
                        b += bt[CB_TO_I(frame[xp + y_offs[i]])];
                    }
                }

                r = MIN(r, 255);
                g = MIN(g, 255);
                b = MIN(b, 255);
                frame_out[y*w+x] = I3_TO_I(r,g,b);
            }

            for (; x < w - x_max; x++) {
                int r = 0, g = 0, b = 0;
                for (int i = 0; i < ictx->n_points; i++) {
                    int xp = x - x_points[i];
                    r += bt[CR_TO_I(frame[xp + y_offs[i]])];
                    g += bt[CG_TO_I(frame[xp + y_offs[i]])];
                    b += bt[CB_TO_I(frame[xp + y_offs[i]])];
                }

                r = MIN(r, 255);
                g = MIN(g, 255);
                b = MIN(b, 255);
                frame_out[y*w+x] = I3_TO_I(r,g,b);
            }

            for (; x < w; x++) {
                int r = 0, g = 0, b = 0;
                for (int i = 0; i < ictx->n_points; i++) {
                    int xp = x - x_points[i];
                    if (xp >= 0 && xp < w) {
                        r += bt[CR_TO_I(frame[xp + y_offs[i]])];
                        g += bt[CG_TO_I(frame[xp + y_offs[i]])];
                        b += bt[CB_TO_I(frame[xp + y_offs[i]])];
                    }
                }

                r = MIN(r, 255);
                g = MIN(g, 255);
                b = MIN(b, 255);
                frame_out[y*w+x] = I3_TO_I(r,g,b);
            }
        } else {
            for (int x = 0; x < w; x++) {
                int r = 0, g = 0, b = 0;
                for (int i = 0; i < ictx->n_points; i++) {
                    int xp = x - x_points[i];
                    if (xp >= 0 && xp < w && y_offs[i] != -1) {
                        r += bt[CR_TO_I(frame[xp + y_offs[i]])];
                        g += bt[CG_TO_I(frame[xp + y_offs[i]])];
                        b += bt[CB_TO_I(frame[xp + y_offs[i]])];
                    }
                }

                r = MIN(r, 255);
                g = MIN(g, 255);
                b = MIN(b, 255);
                frame_out[y*w+x] = I3_TO_I(r,g,b);
            }
        }
    }

    float rotation = ictx->rotation + ictx->rotation_inc + ((ictx->rotation_inc2 -
            ictx->rotation_inc) * s);
    if (rotation > 255.0)
        ictx->rotation = rotation - 255.0;
    else if (rotation < -255.0)
        ictx->rotation = rotation + 255.0;
    else
        ictx->rotation = rotation;

    float status = ictx->status + ictx->speed;
    if (status > M_PI)
        ictx->status = M_PI;
    else if (status < -M_PI)
        ictx->status = M_PI;
    else
        ictx->status = status;

    if (ictx->blend)
        blend_block_add(frame_out, frame_out, frame, w*h);
    else if (ictx->blend_avg)
        blend_block_avg(frame_out, frame_out, frame, w*h);

    return 1;
}

Component t_interferences = {
    .name = "Trans / Interferences",
    .code = 41,
    .priv_size = sizeof(InterferencesContext),
    .load_config = load_config,
    .render = render,
};
