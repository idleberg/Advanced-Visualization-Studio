/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

#include "blend.h"
#include "common.h"
#include "linedraw.h"
#include "pixel.h"

#define SWAP(a,b,temp) \
    (temp) = (a); \
    (a) = (b); \
    (b) = (temp);

/* FIXME: line size needed. */
/* TODO: blend function. */
void line(int *frame, int x1, int y1, int x2, int y2, int width, int height, int colour, int blend_mode)
{
    /* If the colour is black andthe blend mode is additive, nothing will be
     * drawn so just return and skip all the work. */
    if (colour == 0 && blend_mode_get_mode(blend_mode) == BM_ADDITIVE)
        return;

    int diff_y = ABS(y2 - y1);
    int diff_x = ABS(x2 - x1);

    /* If both ends of the line are beyond the same edge of the frame, return. */
    if ((x1 < 0 && x2 < 0) || (x1 > width && x2 > width))
        return;

    if ((y1 < 0 && y2 < 0) || (y1 > height && y2 > height))
        return;

    int line_w = blend_mode_get_width(blend_mode);

    if (!diff_x) { /* Optimise vertical line drawing. */
        x1 -= line_w / 2;

        /* If the line is wholly outside the frame, there is nothing to draw.
         * The order of this condition has been swapped to allow us to return
         * from here and prevent another level of indentation. */
        if (x1 + line_w <=0 || x1 > width)
            return;

        int top = MAX(MIN(y1, y2), 0); /* The top edge of the line. */
        int bot = MIN(MAX(y1, y2), height - 1); /* The bottom edge of the line. */

        if (x1 < 0) {     /* If the left edge is outside the frame: */
            line_w += x1; /* - reduce the line width */
            x1 = 0;       /* - clip the left edge to 0 */
        }

        if (x1 + line_w >= width) /* If the right edge is outside the frame: */
            line_w = width - x1;  /* - set the line width to the remaining space. */

        frame += top * width + x1; /* Advance to the first pixel to be filled. */
        width -= line_w; /* number of pixels between the right edge on one scanline and the left edge on the next. */

        if (line_w <= 0) /* If the line width is zero (or negative) there is nothing to draw. */
            return;

        while (top++ < bot) {
            int x = line_w;

            while (x--) {
                blend_pixel(frame, colour, blend_mode);
                frame++;
            }

            frame += width;
        }

        return;
    } /* End of vertical line. */

    if (!diff_y) { /* Optimise horizontal line drawing. */
        y1 -= line_w / 2;

        /* If the line is wholly outside the frame, there is nothing to draw.
         * As above the order has been swapped. */
        if (y1 + line_w <= 0 || y1 > height)
            return;

        int left = MAX(MIN(x1, x2), 0); /* The left edge of the line. */
        int right = MIN(MAX(x1, x2), width - 1); /* The right edge of the line. */

        if (y1 < 0) {     /* If the top edge is outside the frame: */
            line_w += y1; /* - reduce the line width */
            y1 = 0;       /* - clip the top edge to 0 */
        }

        if (y1 + line_w >= height) /* If the bottom edge is outside the frame: */
            line_w = height - y1;  /* - set the line width to the remaining space. */

        frame += y1 * width + left; /* FIXME: line size needed? */
        width -= right - left; /* FIXME: line size needed? */

        if (line_w <= 0) /* If the line width is zero (or negative) there is nothing to draw. */
            return;

        int y = line_w;

        while (y--) {
            int lt = left;

            while (lt++ < right) {
                blend_pixel(frame, colour, blend_mode);
                frame++;
            }

            frame += width; /* FIXME: line size needed? */
        }

        return;
    } /* End of horizontal line. */

    if (diff_y <= diff_x) { /* x major, low slope. */
        /* First things first.  For better line drawing, let's see if we can't
         * get the width calculated right.
         * lw is the width in pixels. If dy = 0, then lw=lw. If dy=dx, then:
         *
         *     __________ C=30d
         *    |\
         *    | \ lw
         *    |  \_____ A=90d
         * ?  |  /
         *    | /   pc2
         *    |/ ____ B = 60d
         *
         *    tan(C) = sin(C)/cos(C) = (dy / dx)
         *    cos(C) = lw / ?
         *
         *    sin(C)/(lw / ? ) = (dy / dx)
         *    sin(C) * ? / lw = (dy/dx)
         *    sin(C) * ?  = lw * dy/dx;
         *    ? = lw * dy/dx / sin(C)
         *
         *    cos(C) = lw / ?  === ? = lw / cos(C)
         *    tan(C) = dy / dx;
         *
         *    C = atan2(dy,dx);
         *    ? = lw / cos(C)
         *
         */

        if (x2 < x1) {
            int temp;
            SWAP(x1, x2, temp);
            SWAP(y1, y2, temp);
        }

        int yincr = (y2 > y1) ? 1 : -1;
        int offsincr = (y2 > y1) ? width : -width;
        y1 -= line_w / 2;
        int offs = y1 * width + x1;
        int d = diff_y + diff_y - diff_x;
        int Eincr = diff_y + diff_y;
        int NEincr = d - diff_x;

        /* If the line is wholly outside the frame, there is nothing to draw.
         * As above the order has been swapped. */
        if (x2 < 0 || x1 >= width)
            return;

        if (x1 < 0) {
            int v = yincr * -x1;

            if (diff_x)
                v = (v * diff_y) / diff_x;

            y1 += v;
            offs += v * width - x1;
            x1 = 0;
        }

        if (x2 > width)
            x2 = width;

        while (x1 < x2) {
            int yp = y1;
            int ype = y1 + line_w;
            int *newfb = frame + offs;

            if (yp < 0) {
                newfb -= yp * width;
                yp = 0;
            }

            if (ype > height)
                ype = height;

            while (yp++ < ype) {
                blend_pixel(newfb, colour, blend_mode);
                newfb += width;
            }

            if (d < 0)
                d += Eincr;
            else {
                d += NEincr;
                y1 += yincr;
                offs += offsincr;
            }

            offs++;
            x1++;
        }
    } /* End of x major, low slope. */

    else {
        if (y2 < y1) {
            int temp;
            SWAP(x1, x2, temp);
            SWAP(y1, y2, temp);
        }

        int yincr = (x2 > x1) ? 1 : -1;
        int d = diff_x + diff_x - diff_y;
        int Eincr = diff_x + diff_x;
        int NEincr = d - diff_y;
        x1 -= line_w / 2;
        int offs = y1 * width + x1;

        /* If the line is wholly outside the frame, there is nothing to draw.
         * As above the order has been swapped. */
        if (y2 < 0 || y1 >= height)
            return;

        if (y1 < 0) {
            int v = yincr * -y1;

            if (diff_y)
                v = (v * diff_x) / diff_y;

            x1 += v;
            offs += v - y1 * width;
            y1 = 0;
        }

        if (y2 > height)
            y2 = height;

        while (y1 < y2) {
            int xp = x1;
            int xpe = x1 + line_w;
            int *newfb = frame + offs;

            if (xp < 0) {
                newfb -= xp;
                xp = 0;
            }

            if (xpe > width)
                xpe = width;

            while (xp++ < xpe) {
                blend_pixel(newfb, colour, blend_mode);
                newfb++;
            }

            if (d < 0)
                d += Eincr;
            else {
                d += NEincr;
                x1 += yincr;
                offs += yincr;
            }

            offs += width;
            y1++;
        }
    }
}
