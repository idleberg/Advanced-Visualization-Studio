/*
 * Copyright (c) 2015 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* TODO: consider using pb_0 as a memory argument on x86_32. */

#include <emmintrin.h>

#define blend_pixel_add blend_pixel_add_intrinsic
#define blend_pixel_adj blend_pixel_adj_intrinsic
#define blend_pixel_avg blend_pixel_avg_intrinsic
#define blend_pixel_max blend_pixel_max_intrinsic
#define blend_pixel_min blend_pixel_min_intrinsic
#define blend_pixel_mul blend_pixel_mul_intrinsic
#define blend_pixel_sub blend_pixel_sub_intrinsic
#define blend_pixel_4 blend_pixel_4_intrinsic

#define BODY(f) \
    __m128i pixel_a = _mm_cvtsi32_si128(a); \
    __m128i pixel_b = _mm_cvtsi32_si128(b); \
    __m128i pixel_ret = f(pixel_a, pixel_b); \
    return _mm_cvtsi128_si32 (pixel_ret); \

static inline
int blend_pixel_add_intrinsic(int a, int b)
{
    BODY(_mm_adds_epu8)
}

static inline
int blend_pixel_max_intrinsic(int a, int b)
{
    BODY(_mm_max_epu8)
}

static inline
int blend_pixel_min_intrinsic(int a, int b)
{
    BODY(_mm_min_epu8)
}

static inline
int blend_pixel_avg_intrinsic(int a, int b)
{
    BODY(_mm_avg_epu8)
}

static inline
int blend_pixel_sub_intrinsic(int a, int b)
{
    BODY(_mm_subs_epu8)
}

static inline
int blend_pixel_mul_intrinsic(int a, int b)
{
    __m128i zero = _mm_setzero_si128();
    __m128i pixel_a = _mm_unpacklo_epi8(_mm_cvtsi32_si128(a), zero);
    __m128i pixel_b = _mm_unpacklo_epi8(_mm_cvtsi32_si128(b), zero);

    __m128i pixel_ret = _mm_mullo_epi16 (pixel_a, pixel_b);
    pixel_ret = _mm_srli_epi16 (pixel_ret, 8);
    pixel_ret = _mm_packus_epi16(pixel_ret, pixel_ret);
    return _mm_cvtsi128_si32 (pixel_ret);
}

static inline
int blend_pixel_adj_intrinsic(int a, int b, int v)
{
    __m128i zero      = _mm_setzero_si128();
    __m128i pixel_a   = _mm_unpacklo_epi8(_mm_cvtsi32_si128(a), zero);
    __m128i pixel_b   = _mm_unpacklo_epi8(_mm_cvtsi32_si128(b), zero);
    __m128i adj_v     = _mm_shufflelo_epi16(_mm_cvtsi32_si128(v), 0);
    __m128i adj_255_v = _mm_shufflelo_epi16(_mm_cvtsi32_si128(255-v), 0);

    pixel_a = _mm_mullo_epi16(pixel_a, adj_v);
    pixel_b = _mm_mullo_epi16(pixel_b, adj_255_v);

    __m128i pixel_ret = _mm_add_epi16 (pixel_a, pixel_b);
    pixel_ret = _mm_srli_epi16 (pixel_ret, 8);
    pixel_ret = _mm_packus_epi16(pixel_ret, pixel_ret);
    return _mm_cvtsi128_si32 (pixel_ret);
}

static inline
int blend_pixel_4_intrinsic(int *p, int w, int xp, int yp)
{
    __m128i zero     = _mm_setzero_si128();
    __m128i v_xp     = _mm_shufflelo_epi16(_mm_cvtsi32_si128(xp), 0);
    __m128i v_255_xp = _mm_shufflelo_epi16(_mm_cvtsi32_si128(255-xp), 0);

    __m128i p1 = _mm_unpacklo_epi8(_mm_cvtsi32_si128(p[0]), zero);
    __m128i p2 = _mm_unpacklo_epi8(_mm_cvtsi32_si128(p[1]), zero);
    __m128i p3 = _mm_unpacklo_epi8(_mm_cvtsi32_si128(p[w]), zero);
    __m128i p4 = _mm_unpacklo_epi8(_mm_cvtsi32_si128(p[w+1]), zero);

    p1 = _mm_mullo_epi16(p1, v_255_xp);
    p2 = _mm_mullo_epi16(p2, v_xp);
    p3 = _mm_mullo_epi16(p3, v_255_xp);
    p4 = _mm_mullo_epi16(p4, v_xp);

    p1 = _mm_srli_epi16(_mm_add_epi16(p1, p2), 8);
    p2 = _mm_srli_epi16(_mm_add_epi16(p3, p4), 8);

    p1 = _mm_mullo_epi16(p1, _mm_shufflelo_epi16(_mm_cvtsi32_si128(255-yp), 0));
    p2 = _mm_mullo_epi16(p2, _mm_shufflelo_epi16(_mm_cvtsi32_si128(yp), 0));

    p1 = _mm_srli_epi16(_mm_add_epi16(p1, p2), 8);

    return _mm_cvtsi128_si32(_mm_packus_epi16(p1, p1));
}
