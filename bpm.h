/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_BPM
#define AVS_HEADER_BPM 1

#include <stdint.h>

enum BeatConfig {
    BEAT_STANDARD = 1,
    BEAT_ADVANCED = 2,
    BEAT_STICKY = 4,
    BEAT_NO_STICKY = 8,
    BEAT_ONLY_STICKY = 16,
    BEAT_NO_ONLY_STICKY = 32,
    BEAT_NEW_ADAPT = 64,
    BEAT_NEW_RESET = 128,
};

typedef struct BeatContext BeatContext;

void *init_bpm(enum BeatConfig);

int refine_beat(BeatContext *, int, uint32_t);

#endif
