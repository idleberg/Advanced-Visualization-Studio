/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "block.h"
#include "components.h"
#include "common.h"

typedef struct {
    int enabled;
    int only_first;
    int colour;
    int counter;
    int blend, blend_avg;
} ClearScreenContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ClearScreenContext *clear = ctx->priv;
    int pos = 0;
    R32(clear->enabled);
    R32(clear->colour)
    R32(clear->blend);
    R32(clear->blend_avg);
    R32(clear->only_first);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    ClearScreenContext *clear = ctx->priv;

    if (!clear->enabled)
        return 0;
    if (clear->only_first && clear->counter)
        return 0;

    clear->counter++;

    if (clear->blend == 2)
        blend_block_const(frame, frame, clear->colour, w*h, ctx->blend_mode);
    else if (clear->blend)
        blend_block_const_add(frame, frame, clear->colour, w*h);
    else if (clear->blend_avg)
        blend_block_const_avg(frame, frame, clear->colour, w*h);
    else
        blend_block_const(frame, frame, clear->colour, w*h, 0);

    return 0;
}

Component r_clear = {
    .name = "Render / Clear Screen",
    .code = 25,
    .priv_size = sizeof(ClearScreenContext),
    .load_config = load_config,
    .render = render,
};
