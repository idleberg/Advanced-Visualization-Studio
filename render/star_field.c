/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int x, y;
    float z, speed;
    //int ox, oy; /* these appear to be unused. */
} StarFormat;

typedef struct {
    int enabled;
    int colour;
    int max_stars, max_stars_set;
    //int off_x, off_y, off_z; /* these seem to be pointless and z is unused. */
    float warp_speed;
    int blend, blend_avg;
    StarFormat stars[4096];
    int width, height;
    int on_beat;
    float speed_beat;
    float inc_beat;
    int dur_frames;
    float current_speed;
    int nc;
} StarfieldContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    StarfieldContext *star = ctx->priv;
    int pos = 0;
    R32(star->enabled);
    R32(star->colour);
    R32(star->blend);
    R32(star->blend_avg);
    R32F(star->warp_speed);
    R32(star->max_stars_set);
    R32(star->on_beat);
    R32F(star->speed_beat);
    R32(star->dur_frames);
    star->current_speed = star->warp_speed;
    return 0;
}

static void initialize_stars(StarfieldContext *ctx, int w, int h)
{
    uint64_t temp = ctx->max_stars_set * (uint64_t)(ctx->width * ctx->height);
    temp = (temp + ((512*384)/2)) / (512*384);
    ctx->max_stars = MAX(4095, temp);
    for (int i = 0; i < ctx->max_stars; i++) {
        ctx->stars[i].x = (rand() % w) - w/2;
        ctx->stars[i].y = (rand() % h) - h/2;
        ctx->stars[i].z = rand() & 255;
        ctx->stars[i].speed = (rand() % 9 + 1) / 10.0;
    }
}

static void create_star(StarfieldContext *ctx, int w, int h, int i)
{
    ctx->stars[i].x = rand() % w - w/2;
    ctx->stars[i].y = rand() % h - h/2;
    ctx->stars[i].z = 255;
}

static inline int blend_adapt(int a, int b, int divisor)
{
    return (((a >> 4) & 0x0f0f0f) * (16 - divisor)) + (((b >> 4) & 0x0f0f0f) * divisor);
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    StarfieldContext *star = ctx->priv;

    if (!star->enabled)
        return 0;

    if (star->on_beat && is_beat) {
        star->current_speed = star->speed_beat;
        star->inc_beat = (star->warp_speed - star->current_speed) / star->dur_frames;
        star->nc = star->dur_frames;
    }

    if (w != star->width || h != star->height) {
        star->width = w;
        star->height = h;
        initialize_stars(star, w, h);
    }

    for (int i = 0; i < star->max_stars; i++) {
        if (star->stars[i].z < 0) {
            create_star(star, w, h, i);
            continue;
        }

        int nx = (star->stars[i].x << 7) / star->stars[i].z + w/2;
        int ny = (star->stars[i].y << 7) / star->stars[i].z + h/2;

        if (nx < 0 || nx >= w || ny < 0 || ny >= h) {
            create_star(star, w, h, i);
            continue;
        }

        int c = (255 - star->stars[i].z) * star->stars[i].speed;

        if (star->colour != 0xffffff)
            c = blend_adapt(I3_TO_I(c,c,c), star->colour, c >> 4);
        else
            c = I3_TO_I(c,c,c);

        if (star->blend)
            frame[ny * w + nx] = blend_pixel_add(frame[ny * w + nx], c);
        else if (star->blend_avg)
            frame[ny * w + nx] = blend_pixel_avg(frame[ny * w + nx], c);
        else
            frame[ny * w + nx] = c;

        star->stars[i].z -= star->stars[i].speed * star->current_speed;
    }

    if (star->nc) {
        star->current_speed = MAX(0, star->current_speed + star->inc_beat);
        star->nc--;
    } else
        star->current_speed = star->warp_speed;

    return 0;
}

Component r_starfield = {
    .name = "Render / Starfield",
    .code = 27,
    .priv_size = sizeof(StarfieldContext),
    .load_config = load_config,
    .render = render,
};
