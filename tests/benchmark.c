/*
 * Copyright (c) 2014-2015 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if SYS_WINDOWS || SYS_CYGWIN
#include <windows.h>
#undef ERROR /* Windows defines this somewhere so this silences a warning. */
#elif SYS_LINUX
#define _XOPEN_SOURCE 700
#include <time.h>
#endif

#include <inttypes.h>
#include <getopt.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>


#include "avs.h"
#include "common.h"

enum  {
    OPT_FILE_AUDIO = 'a',
    OPT_FILE_INPUT = 'i',
    OPT_FRAMES = 'f',
    OPT_RUNS = 'r',
    OPT_SIZE = 's',
    OPT_VERBOSE = 'v',
};

struct TestOptions {
    char *file_audio;
    char *file_input;
    int width, height;
    int runs;
    int verbose;
    int frames;
};

struct channels{
    uint8_t left[576], right[576], center[576];
};

static void default_options(struct TestOptions *opt)
{
    memset(opt, 0, sizeof(struct TestOptions));
    opt->width = 320;
    opt->height = 240;
}

static int64_t get_time(void)
{
#if SYS_WINDOWS || SYS_CYGWIN
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return li.QuadPart;
#elif SYS_LINUX
    struct timespec t;
    if (clock_gettime(CLOCK_MONOTONIC_RAW, &t))
        return 1;
    return t.tv_sec * ((int64_t)1000000000) + t.tv_nsec;
#else
    return 1;
#endif
}

static int64_t get_time_divisor(void)
{
#if SYS_WINDOWS || SYS_CYGWIN
    LARGE_INTEGER li;
    QueryPerformanceFrequency(&li);
    return li.QuadPart;
#elif SYS_LINUX
    return 1000000000;
#else
    return 1;
#endif
}

static int parse_opts(int argc, char ** argv, struct TestOptions *options)
{
    struct option cmd_options[] = {
        { "audio",  required_argument, 0, OPT_FILE_AUDIO },
        { "frames", required_argument, 0, OPT_FRAMES },
        { "input",  required_argument, 0, OPT_FILE_INPUT },
        { "runs",   required_argument, 0, OPT_RUNS },
        { "size",   required_argument, 0, OPT_SIZE },
        { "verbose",      no_argument, 0, OPT_SIZE },
        { 0 }
    };

    while (1) {
        int option_index;
        int arg = getopt_long(argc, argv, "a:f:i:r:s:v", cmd_options, &option_index);

        if (arg == -1)
            break;

        switch (arg) {
            case OPT_FILE_AUDIO: {
                options->file_audio = optarg;
            } break;

            case OPT_FILE_INPUT: {
                options->file_input = optarg;
            } break;

            case OPT_SIZE: {
                const char *sub;
                int w = 0, h = 0;

                w = strtol(optarg, (void*)&sub, 10);
                if (*sub)
                    sub++;
                h = strtol(sub, (void*)&sub, 10);

                if (w <= 0 || h <= 0) {
                    ERROR("Invalid size given: %s\n", optarg);
                    return -1;
                }

                options->width = w;
                options->height = h;
            } break;

            case OPT_VERBOSE: {
                options->verbose = 1;
            } break;

            case OPT_FRAMES: {
                options->frames = atoi(optarg);
            } break;

            default:
                //fprintf(stderr, "Error: Unknown option: %s\n", argv[option_index]);
                return -1;
        }
    }

    if (!options->file_input) {
        ERROR("No input file given\n");
        return -1;
    }

    if (!options->file_audio) {
        ERROR("No audio file given\n");
        return -1;
    }

    return 0;
}

static void sine_wave(uint8_t data[576], int amplitude, int periods)
{
    for (int i = 0; i < 576; i++)
        data[i] = sin((i * periods * 2 * M_PI)/575.0) * amplitude + 128.0;
}

static int read_audio(FILE *audio, struct channels *waveform)
{
    uint8_t data[576*2];
    size_t count = fread(data, 1, 576*2, audio);
    int not_end = 1;

    if (count < 576*2) {
        memset(data + count, 0, 576*2 - count);
        not_end = 0;
    }

    for (int i = 0; i < 576; i++) {
        int l = data[2*i+0];
        int r = data[2*i+1];
        int c = (l+r+1)/2;

        waveform->left[i] = l;
        waveform->right[i] = r;
        waveform->center[i] = c;
    }

    return not_end;
}

int main(int argc, char **argv)
{
    struct TestOptions options;
    struct channels waveform;
    uint8_t spectrum[576];

    AVSContext *actx = 0;
    AVSDataContext avsdc = {
        .waveform = { waveform.left, waveform.right, waveform.center },
        .spectrum = { spectrum, spectrum, spectrum },
    };

    FILE *audio = 0;
    int ret = 0;
    int64_t time_total = 0;
    int64_t time_divisor = get_time_divisor();

    default_options(&options);

    ret = parse_opts(argc, argv, &options);
    if (ret)
        goto error;

    audio  = fopen(options.file_audio, "rb");
    if (!audio) {
        ERROR("error opening audio file: '%s'\n", options.file_audio);
        ret = 1;
        goto error;
    }

    avsdc.width = options.width;
    avsdc.height = options.height;

    ret = avs_init(&actx, options.file_input, 0);
    if (ret)
        goto error;

    memset(spectrum,      51, 128);
    memset(spectrum+128, 102, 128);
    memset(spectrum+256, 154, 128);
    memset(spectrum+384, 204, 128);

    int frame_count = 0;

    while (read_audio(audio, &waveform)) {
        int64_t time_before = get_time();

        ret = avs_render_frame(actx, &avsdc, 1);
        if (ret)
            break;

        int64_t time_after = get_time();
        time_total += time_after - time_before;
        frame_count++;

        if (options.verbose) {
            VERBOSE("time taken: %" PRId64 "µs, frame: %d        \r",
                    ((time_after - time_before) * 1000000) / time_divisor,
                    frame_count);
#if SYS_WINDOWS
            fflush(stderr);
#endif
        }

        if (options.frames && frame_count > options.frames)
            break;
    };

    if (ret || !frame_count)
        goto error;

    double duration = time_total / (double)time_divisor;
    CONT("\n");
    INFO("time taken: %.1fs, fps: %.1f, frames: %d, avg frame time: %" PRId64 "µs\n",
            duration, frame_count / duration, frame_count,
            (time_total * 1000000) / (frame_count * time_divisor));
error:
    avs_uninit(actx);

    if (audio)
        fclose(audio);

    return ret;
}
