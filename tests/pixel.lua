local function new_stats()
    return {
        min = 1000000000,
        max = -1000000000,
        sum = 0,
        s2 = 0,
        num = 0,
        update = function(self, value)
            self.min = math.min(self.min, value);
            self.max = math.max(self.max, value);
            self.sum = self.sum + value;
            self.s2  = self.s2 + (value * value);
            self.num = self.num + 1;
        end
    };
end

local acceptable_stats = {
    add = { s2 = 0 },
    sub = { s2 = 0 },
    max = { s2 = 0 },
    min = { s2 = 0 },
    avg = { s2 = 8192 },
};

for _,entry in ipairs(pixel_ops) do
    local func_c, func_i, op = entry.c, entry.i, entry.op;

    io.write(("Testing %s...\n"):format(op));

    local func_r;
    local stats_c, stats_i = new_stats(), new_stats();
    --local mismatch = false;

    if op:match("adj") then
        func_r = function(a,b,c) return ((a*c)+(b*(255-c)))/255 end;

        for a = 0, 255 do
            for b = 0, 255 do
                for c = 0, 255 do
                    local ref = func_r(a,b,c);
                    local val_c = func_c(a,b,c);
                    local val_i = func_i(a,b,c);
                    stats_c:update(val_c - ref);
                    stats_i:update(val_i - ref);
                end
            end
        end
    elseif op:match("pix4") then
        func_r = function(a,b,c,d,xp,yp)
            a = a * (255-xp) * (255-yp)
            b = b * (xp) * (255-yp)
            c = c * (255-xp) * (yp)
            d = d * (xp) * (yp)
            return (a+b+c+d)/(255*255)
        end

        local samples = {0, 1, 2, 4, 42, 127, 128, 144, 251, 253, 254, 255}
        for _,a in ipairs(samples) do
            for _,b in ipairs(samples) do
                for _,c in ipairs(samples) do
                    for _,d in ipairs(samples) do
            for _,xp in ipairs(samples) do
                for _,yp in ipairs(samples) do
                    local ref = func_r(a,b,c,d,xp,yp);
                    local val_c = func_c(a,b,c,d,xp,yp);
                    local val_i = func_i(a,b,c,d,xp,yp);
                    stats_c:update(val_c - ref);
                    stats_i:update(val_i - ref);
                end
            end
                    end
                end
            end
        end
    else
        if op:match("add") then
            func_r = function(a,b) return math.min(a+b,255) end;
        elseif op:match("avg") then
            func_r = function(a,b) return (a+b)/2 end;
        elseif op:match("max") then
            func_r = function(a,b) return math.max(a,b) end;
        elseif op:match("min") then
            func_r = function(a,b) return math.min(a,b) end;
        elseif op:match("sub") then
            func_r = function(a,b) return math.max(a-b,0) end;
        elseif op:match("mul") then
            func_r = function(a,b) return (a*b)/255 end;
        else
            error("unknown op");
        end

        for a = 0, 255 do
            for b = 0, 255 do
                local ref = func_r(a,b);
                local val_c = func_c(a,b);
                local val_i = func_i(a,b);
                stats_c:update(val_c - ref);
                stats_i:update(val_i - ref);
            end
        end
    end

    stats_c.sum = stats_c.sum / stats_c.num
    stats_c.s2  = stats_c.s2 / stats_c.num
    stats_i.sum = stats_i.sum / stats_i.num
    stats_i.s2  = stats_i.s2 / stats_i.num

    local fmt_str = "\t%s: min = %f, max = %f, avg = %f, rms = %f\n";
    io.write(fmt_str:format("C", stats_c.min, stats_c.max, stats_c.sum,
        math.sqrt(stats_c.s2)));
    io.write(fmt_str:format("I", stats_i.min, stats_i.max, stats_i.sum,
        math.sqrt(stats_i.s2)));

        --[[
    if acceptable_stats[op] ~= nil then
        local as = acceptable_stats[op];
        if as.s2 == stats_c.s2 then
            io.write("\tc function OK\n");
        end
        if as.s2 == stats_i.s2 then
            io.write("\tintrinsic function OK\n");
        end
    end
    ]]
end
