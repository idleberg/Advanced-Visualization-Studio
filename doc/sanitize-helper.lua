local subs = {
    -- Replace semi-colons, new lines and spaces (the %s character class) with a
    -- single semi-colon and a single newline character.  This serves the dual
    -- purpose of removing multiple semi-colons as well as prettying the string
    -- when it is printed in a terminal.
    { [=[;+%s*]=],                   [=[;\n]=] },

    -- Remove C-style inline comments.
    { [=[/%*.-%*/]=],                    [=[]=] },

    -- Remove C-style line comments.
    { [=[//.-[\n]]=],                    [=[]=] },

    -- Replace (g)megabuf usage with tables.
    { [=[megabuf%(([%d%a]+)%)]=],        [=[megabuf[%1]]=] },

    -- Replace register usage with a table.
    { [=[reg(%d%d)]=],                   [=[global_registers[%1]]=] },

    -- Try to fix the weird assign semantics.
    { [=[assign%(([^,]+),(.-)%)[;\n]]=], [=[%1=%2;]=] },

    -- Replace if usage with the shim.
    { [=[if%(]=],                        [=[__avs_if(]=] },

    -- Replace $PI with an available feature.
    { [=[$[Pp][Ii]]=],                   [=[math.pi]=] },

    -- Replace $E with an available feature.
    { [=[$[Ee]]=],                       [=[math.e]=] },

    -- Replace $PHI with an available feature.
    { [=[$[Pp][Hh][Ii]]=],               [=[math.phi]=] },
}

local str = ...

-- A hack to work around strings in one preset starting with a semi-colon.
-- TODO: a better and more flexible solution.
if string.sub(str, 1, 1) == [=[;]=] then
    str = string.sub(str, 2)
end

for _, sub in ipairs(subs) do
    str = string.gsub(str, sub[1], sub[2])
end

if string.sub(str, -2) ~= [=[;\n]=] then
    str = str .. [=[;\n]=]
end

return str
