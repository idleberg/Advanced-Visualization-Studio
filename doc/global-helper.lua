-- Global Megabuffer
gmegabuf = {}
setmetatable(gmegabuf, {
    __newindex = function(t, k, v) return __avs_global_megabuf_function(k, v) end,
    __index = function(t, k) return __avs_global_megabuf_function(k) end
})

-- Global registers
global_registers = {}
setmetatable(global_registers, {
    __newindex = function(t, k, v) return __avs_global_registers_function(k, v) end,
    __index = function(t, k) return __avs_global_registers_function(k) end
})

-- Local megabuffer
megabuf = {}
setmetatable(megabuf, {__index = function() return 0.0 end})

-- Lua can be made to initialise new variables as zero when they are first used,
-- which is what Nullsoft's eel code does.  A massive hat-tip to the people on
-- the lua channel on Freenode for this, in particular 'doub' and 'LU324'.
setmetatable(_G, {__index = function() return 0.0 end})
